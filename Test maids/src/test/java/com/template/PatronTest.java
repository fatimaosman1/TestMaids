package com.template;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.template.Model.Main.Patron;
import com.template.Model.Main.Patron;
import com.template.Repository.Main.PatronRep;
import com.template.Service.Main.PatronService;
import com.template.Service.Main.PatronService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.hamcrest.CoreMatchers.is;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.willDoNothing;
import static org.mockito.Mockito.times;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * this class is to test Patron end point .
 */
@RunWith(MockitoJUnitRunner.Silent.class)
public class PatronTest {
    @Mock
    private PatronRep patronrep;
    @InjectMocks
    private PatronService patronService;


    @org.junit.Test
    public void getPatronById() throws Exception{

        // given - precondition or setup
        int Patronid = 1;
        Patron patron = new Patron();
        patron.setName("test");
        patron.setContact("4637687697");

        Mockito.when(patronService.save(patron)).thenReturn(patron);
        Mockito.when(patronrep.findById(Patronid)).thenReturn(Optional.of(patron));

        // act
        Patron result = patronService.findPatronById(Patronid).get();

        // assert
        assertThat(result).isEqualTo(patron);
        Mockito.verify(patronrep, times(1)).findById(Patronid);

    }
    @org.junit.Test
    public void testGetAllPatrons() {
        // arrange
        Patron patron = new Patron();
        patron.setName("test");
        patron.setContact("4637687697");

        Patron patron1 = new Patron();
        patron.setName("test");
        patron.setContact("4637687697");
        List<Patron> Patrons = Arrays.asList(
                patron,
                patron1
        );
        Mockito.when(patronrep.findAll()).thenReturn(Patrons);
        // act
        List<Patron> result = patronService.findAll();
        // assert
        assertThat(result).isEqualTo(Patrons);
    }


    @Test
    public void testAddPatron() {
        // arrange
        Patron patron = new Patron();
        patron.setName("test");
        patron.setContact("4637687697");

        // act
        patronService.save(patron);

        // assert
        ArgumentCaptor<Patron> captor = ArgumentCaptor.forClass(Patron.class);
        Mockito.verify(patronrep).save(captor.capture());
        assertThat(captor.getValue().getName()).isEqualTo(patron.getName());
        assertThat(captor.getValue().getContact()).isEqualTo(patron.getContact());

    }

}
