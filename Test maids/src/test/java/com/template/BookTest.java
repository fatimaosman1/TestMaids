package com.template;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.template.Model.Main.Book;
import com.template.Repository.Main.BookRep;
import com.template.RestController.BookControler;
import com.template.Service.Main.BookService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.*;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.BDDMockito.*;
import static org.hamcrest.CoreMatchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;

/**
 * this class is to test Book end point .
 */

@RunWith(MockitoJUnitRunner.Silent.class)
public class BookTest {
    @Mock
    private BookRep bookrep;
    @InjectMocks
    private BookService  bookService;


    @Test
    public void getBookById() throws Exception{

        // given - precondition or setup
        int bookid = 1;
        Book book = new Book();
        book.setTitle("test");
        book.setISBN("3421");
        book.setAuthor("test");
        book.setYear(2020);
        book.setIsborrowing(false);
        Mockito.when(bookService.save(book)).thenReturn(book);
        Mockito.when(bookrep.findById(bookid)).thenReturn(Optional.of(book));

        // act
        Book result = bookService.findBookById(bookid).get();

        // assert
        assertThat(result).isEqualTo(book);
        Mockito.verify(bookrep, times(1)).findById(bookid);

    }
    @Test
    public void testGetAllBooks() {
        // arrange
        Book book = new Book();
        book.setTitle("test");
        book.setISBN("3421");
        book.setAuthor("test");
        book.setYear(2020);
        book.setIsborrowing(false);

        Book book1 = new Book();
        book1.setTitle("test");
        book1.setISBN("3421");
        book1.setAuthor("test");
        book1.setYear(2020);
        book1.setIsborrowing(false);
        List<Book> Books = Arrays.asList(
                book,
                book1
        );
        Mockito.when(bookrep.findAll()).thenReturn(Books);
        // act
        List<Book> result = bookService.findAll();
        // assert
        assertThat(result).isEqualTo(Books);
    }


    @Test
    public void testAddBook() {
        // arrange
        Book book = new Book();
        book.setTitle("test");
        book.setISBN("3421");
        book.setAuthor("test");
        book.setYear(2020);
        book.setIsborrowing(false);

        // act
        bookService.save(book);

        // assert
        ArgumentCaptor<Book> captor = ArgumentCaptor.forClass(Book.class);
        Mockito.verify(bookrep).save(captor.capture());
        assertThat(captor.getValue().getAuthor()).isEqualTo(book.getAuthor());
        assertThat(captor.getValue().getYear()).isEqualTo(book.getYear());
        assertThat(captor.getValue().getTitle()).isEqualTo(book.getTitle());
        assertThat(captor.getValue().getISBN()).isEqualTo(book.getISBN());
    }




}
