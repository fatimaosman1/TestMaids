package com.template.DataBase.Seeders;



import org.apache.juli.logging.Log;
import org.apache.juli.logging.LogFactory;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;

import com.template.DataBase.Seeder;
import com.template.Model.Basic.Role;
import com.template.Model.Basic.User;

import com.template.Repository.Basic.RoleActionRep;
import com.template.Repository.Basic.RoleRep;
import com.template.Repository.Basic.UsersRep;


public class UserSeeder {

	private final Log logger = LogFactory.getLog(Seeder.class);
	
	public UserSeeder() {

		logger.info("	==>Seeder ==> "+ getClass().getSimpleName());
	}
	
	public void run(RoleRep roleRep,UsersRep userRep,RoleActionRep roleActionRep) {
		Role role;
		String password;
		password=new BCryptPasswordEncoder().encode("admin");
		User user;
		user=new User( "admin", password);
		userRep.save(user);
		user=userRep.findByusername("admin");
		role=roleRep.findById(1).get();
		user.setUser_role(role);
		userRep.save(user);

	}
	
}
