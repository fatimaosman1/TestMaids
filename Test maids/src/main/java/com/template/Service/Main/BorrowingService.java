package com.template.Service.Main;

import com.template.Exception.NoDataFoundException;
import com.template.Model.Main.Book;
import com.template.Model.Main.Borrowing;
import com.template.Model.Main.Patron;
import com.template.Repository.Main.BorrowingRep;
import com.template.Repository.Main.PatronRep;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.template.Exception.MethodNotAllowedException;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class BorrowingService {

	@Autowired
	private BorrowingRep BorrowingRep;
	@Autowired
	BookService bookService;
	@Autowired
	private PatronRep PatronRep;

	/**
	 * get an instance of  this entity <Borrowing> record from database based on it's patronid and bookid.
	 * @param patronid: entity<Patron> id
	 * @param bookid: entity<Book>
	 * @return an instance of entity Patron
	 */
	public Optional<Borrowing> findBorrowingById(int bookid,int patronid) {
		return  BorrowingRep.findtbyBookANAndPatron(bookid,patronid);
	}


	/**
	 * this function to update  Borrowing if book is not Borrowing and exist.
	 * @param patronid: entity<Patron> id
	 * @param bookid: entity<Book>
	 * @return an instance of entity Borrowing
	 */
	public void update(int bookid,int patronid) throws NoDataFoundException, ParseException {
		Book book=bookService.findBookById(bookid)
				.orElseThrow(()-> new NoDataFoundException("book not found"));
		Patron patron=PatronRep.findById(patronid)
				.orElseThrow(()-> new NoDataFoundException("Patron not found"));
		Borrowing borrowing=this.findBorrowingById(bookid,patronid)
				.orElseThrow(()-> new NoDataFoundException("Borrowing not found"));
		if(book.getIsborrowing()){
			SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
			String date = simpleDateFormat.format(new Date());
			 BorrowingRep.updateBorrowingdateReturen( new SimpleDateFormat("dd-MM-yyyy").parse(date),bookid,patronid);
		}else {
			throw new NoDataFoundException("the book is not Borrowing");
		}

	}


	/**
	 * this function to create a new Borrowing and update status the book will borrow  .
	 * @param patronid: entity<Patron> id
	 * @param bookid: entity<Book>
	 * @return an instance of entity Borrowing
	 */
	public Borrowing save(int bookid,int patronid)  throws NoDataFoundException, ParseException {
		Book book=bookService.findBookById(bookid)
				.orElseThrow(()-> new NoDataFoundException("book not found"));
		Patron patron=PatronRep.findById(patronid)
				.orElseThrow(()-> new NoDataFoundException("Patron not found"));
		if(!book.getIsborrowing()){
			SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
			String date = simpleDateFormat.format(new Date());

			Borrowing borrowing=new Borrowing();
			borrowing.setBook(book);
			borrowing.setPatron(patron);
			borrowing.setDateBorrowing( new SimpleDateFormat("dd-MM-yyyy").parse(date));
			book.setIsborrowing(true);
			bookService.update(book,bookid);
			return BorrowingRep.save(borrowing);
		}else {
			throw  new NoDataFoundException("the Book is Borrowing");
		}

	}





	
	
	}
	
	

