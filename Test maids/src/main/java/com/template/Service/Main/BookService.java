package com.template.Service.Main;

import com.template.Exception.NoDataFoundException;
import com.template.Model.Main.Book;
import com.template.Repository.Main.BookRep;
import com.template.Service.Basic.LogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class BookService {

	@Autowired
	private BookRep bookRep;


	/**
	 * get all this entity <Book> record from database.
	 * @return a List<Book>
	 */

	@Cacheable(value = "allbookcache")
	public List<Book> findAll() {
		List<Book> list = bookRep.findAll();
		return list;
	}

	/**
	 * get an instance of  this entity <Book> record from database based on it's ID.
	 * @param id: entity<Book> id
	 * @return an instance of entity Patron
	 */
	@Cacheable(value = "allbookcache", key = "#id")
	public Optional<Book> findBookById(int id) {
		return  bookRep.findById(id);
	}


	/**
	 * this function to update  Book .
	 * @param book: entity<Book>
	 * @param id: entity<Book> id
	 * @return an instance of entity Book
	 */
	@CachePut(value = "allbookcache", key = "#id")
	public Book update(Book book,int id) {
		book.setID(id);
		return bookRep.save(book);
	}

	/**
	 * this function to create a new Book .
	 * @param book: entity<Book>
	 * @return an instance of entity Book
	 */
	@CacheEvict(cacheNames = "allbookcache", allEntries = true)
	public Book save(Book book) {book.setIsborrowing(false) ; return bookRep.save(book);}


	/**
	 * this function to delete Book .
	 * @param id: entity<Book> id
	 */
	@CacheEvict(value = "allbookcache", key = "#id")
		public void delete(int id) {
		Book book = bookRep.findById(id)
				.orElseThrow(() -> new NoDataFoundException("Book not found with id : " + id));
		bookRep.delete(book);
		}
	
	
	}
	
	

