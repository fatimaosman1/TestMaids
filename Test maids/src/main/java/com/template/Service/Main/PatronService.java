package com.template.Service.Main;

import com.template.Exception.NoDataFoundException;
import com.template.Model.Main.Patron;
import com.template.Repository.Main.PatronRep;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class PatronService {

	@Autowired
	private PatronRep PatronRep;

	/**
	 * get all this entity <Patron> record from database.
	 * @return a List<Patron>
	 */
	@Cacheable(value = "allPatroncache")
	public List<Patron> findAll() {
		List<Patron> list = PatronRep.findAll();
		return list;
	}

	/**
	 * get an instance of  this entity <Patron> record from database based on it's ID.
	 * @param id: entity<Patron> id
	 * @return an instance of entity Patron
	 */
	@Cacheable(value = "allPatroncache", key = "#id")
	public Optional<Patron> findPatronById(int id) {
		return  PatronRep.findById(id);
	}


	/**
	 * this function to update  Patron .
	 * @param id: entity<Patron> id
	 * @param Patron: entity<Patron>
	 * @return an instance of entity Patron
	 */
	@CachePut(value = "allPatroncache", key = "#id")
	public Patron update(Patron Patron,int id) {
		Patron.setID(id);
		return PatronRep.save(Patron);
	}

	/**
	 * this function to create a new Patron .
	 * @param Patron: entity<Patron>
	 * @return an instance of entity Patron
	 */
	@CacheEvict(cacheNames = "allPatroncache", allEntries = true)
	public Patron save(Patron Patron) {return PatronRep.save(Patron);}


	/**
	 * this function to delete Patron .
	 * @param id: entity<Patron> id
	 */
	@CacheEvict(value = "allPatroncache", key = "#id")
		public void delete(int id) {
		Patron Patron = PatronRep.findById(id)
				.orElseThrow(() -> new NoDataFoundException("Patron not found with id : " + id));
		PatronRep.delete(Patron);
		}
	
	
	}
	
	

