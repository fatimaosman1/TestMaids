package com.template.Service.Basic;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.template.Model.Basic.RoleAction;
import com.template.Repository.Basic.RoleActionRep;
import com.template.Repository.Basic.RoleRep;
import com.template.Repository.Basic.ActionsRep;





@Service

public class RoleActionService {
	@Autowired
	public LogService logActionService;
	@Autowired
	private RoleActionRep roleActionRep;

	@Autowired
	private ActionsRep actionsRep;
	@Autowired
	private RoleRep roleRep;
	
	public List<RoleAction> getRoleActionByRoleId(int id) {
		List<RoleAction> roleactionList= roleActionRep.getRoleActionByRoleId(id);
		return roleactionList;
	}
	public void create(int roleId , int actionId) {
		RoleAction roleAction = new RoleAction();
		roleAction.setRole(roleRep.findById(roleId).get());
		roleAction.setAction(actionsRep.findById(actionId).get());
		roleAction.setStatus(1);
		roleAction.setCanDelete(1);
		roleActionRep.save(roleAction);

	}
	public RoleAction delete(int id) {
		RoleAction roleAction = roleActionRep.findById(id).get();
		roleAction.setStatus(0);
		roleActionRep.delete(roleAction);

		return roleAction;
		
	}


}
