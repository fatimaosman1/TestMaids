package com.template.Service.Basic;


import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.template.Exception.NoDataFoundException;
import com.template.Model.Basic.Role;
import com.template.Repository.Basic.RoleRep;


@Service
public class RoleService  {

	@Autowired
	public LogService logActionService;

	
	@Autowired
	private RoleRep roleRep;
	
	public List<Role> findAll() {
		List<Role> roleList = roleRep.findAll();
		return roleList;
	}
	public Role roleSave(Role role) {
		role.setStatus(1);
		roleRep.save(role);
		return role ;
	}
	public void delete(int id) {
		Role role = roleRep.findById(id).get();
		role.setStatus(0);
		roleRep.save(role);
	}
	

	
	public Role findRoleById(int id) {
		Role role = roleRep.findRoleById(id);
		if(role == null)
			throw new NoDataFoundException("No Role found with id : "+id);
		return role;
	}
	

	
	
	
	
	
	
	
	
	
	
	

	






}
