package com.template.Service.Basic;


import java.util.Arrays;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.template.Exception.NoDataFoundException;
import com.template.Model.Basic.User;
import com.template.Repository.Basic.RoleRep;
import com.template.Repository.Basic.UsersRep;


@Service
public class UserService implements UserDetailsService {

	@Autowired
	public LogService logActionService;
	@Autowired
	private UsersRep userRep;
	@Autowired
	RoleRep roleRep;

	public User findById(int id) {
		return userRep.findById(id).get();
	}
	public User findByUserName(String username) {
		return userRep.findByusername(username);
	}
	public User save(User user) {
		return userRep.save(user);
	}


	@Transactional
	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		User user = userRep.findByusername(username);
		GrantedAuthority authority = new SimpleGrantedAuthority(user.getUser_role().getRole_name());
		if (user == null)
			throw new NoDataFoundException("No User Found");
		return new org.springframework.security.core.userdetails.User(user.getUsername(), user.getPassword(), Arrays.asList(authority));

	}


}
