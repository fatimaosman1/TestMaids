package com.template.Service.Basic;


import java.time.ZoneId;
import java.sql.Date;
import java.util.List;
import java.time.LocalDateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;
import com.template.Repository.Basic.ActionsRep;
import com.template.Repository.Basic.UsersRep;
import com.template.Model.Basic.ExceptionLogAction;
import com.template.Repository.Basic.ExceptionLogActionRep;

@Service
public class LogService {

	@Autowired
	private ExceptionLogActionRep exceptionLogActionRep;
	public void addExLogAction(int action_index, String param) {
		ExceptionLogAction a = null;
		a = new ExceptionLogAction(
				Date.from(LocalDateTime.now().atZone(ZoneId.systemDefault()).toInstant()), action_index, param);

		exceptionLogActionRep.save(a);

	}
}
