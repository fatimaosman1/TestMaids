package com.template.Service.Basic;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.template.Exception.NoDataFoundException;
import com.template.Model.Basic.Action;
import com.template.Repository.Basic.ActionsRep;

@Service
public class ActionService  {

	@Autowired
	public LogService logActionService;
	@Autowired
	private ActionsRep actionsRep;
	
	public List<Action> findAll() {
		List<Action> actionList = actionsRep.findAll();
		

		return actionList;
	}
	
	public Action findActionById(int id) {
		Action action = actionsRep.findActionById(id);
		if(action == null)
			throw new NoDataFoundException("No action with ID : "+id);
		return action;
	}
	public Action save(Action action) {

		return actionsRep.save(action);
	}
	
	
	}
	
	

