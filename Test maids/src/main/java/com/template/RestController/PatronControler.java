package com.template.RestController;

import com.template.Aspect.RestLog;
import com.template.Exception.InternalServerErrorException;
import com.template.Model.Main.Patron;
import com.template.Service.Main.PatronService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

/**
 * this class is to define the Patron Controller Requests.
 */
@RestController
@RequestMapping("/api")
public class PatronControler {

	@Autowired
	PatronService patronService;


	@RestLog(uri = "/api/patrons")
	@GetMapping(value="/patrons",produces="application/json")
	@PreAuthorize("hasAuthority('ADMIN')")
	public ResponseEntity<List<Patron>> getAllPatrons() {
		try {
			List<Patron> Patrons = patronService.findAll();;
			if (Patrons.isEmpty()) {
				return new ResponseEntity<>(HttpStatus.NO_CONTENT);
			}
			return new ResponseEntity<>(Patrons, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@RestLog(uri = "/api/patrons/{id}")
	@GetMapping(value="/patrons/{id}",produces="application/json")
	@PreAuthorize("hasAuthority('ADMIN')")
	public ResponseEntity<Patron> getPatronById(@PathVariable("id") int id) {
		Optional<Patron> PatronData = patronService.findPatronById(id);
		if (PatronData.isPresent()) {
			return new ResponseEntity<>(PatronData.get(), HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}


	@RestLog(uri = "/api/patrons")
	@PostMapping("/patrons")
	@PreAuthorize("hasAuthority('ADMIN')")
	public ResponseEntity<Patron> createPatron(@RequestBody Patron patron) {
		try {
			return new ResponseEntity<>(patronService.save(patron), HttpStatus.CREATED);
		} catch (Exception e) {
			throw new InternalServerErrorException(e.getMessage());
		}
	}

	@RestLog(uri = "/api/patrons/{id}")
	@PutMapping("/patrons/{id}")
	@PreAuthorize("hasAuthority('ADMIN')")
	public ResponseEntity<Patron> updatePatron(@PathVariable("id") int id, @RequestBody Patron patron) {
		Optional<Patron> PatronData = patronService.findPatronById(id);
		if (PatronData.isPresent()) {
			return new ResponseEntity<>(patronService.update(patron,id), HttpStatus.OK);		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}

	@RestLog(uri = "/api/patrons/{id}")
	@DeleteMapping("/patrons/{id}")
	@PreAuthorize("hasAuthority('ADMIN')")
	public ResponseEntity<HttpStatus> deletePatron(@PathVariable("id") int id) {
		try {
			patronService.delete(id);
			return new ResponseEntity<>(HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}


}
