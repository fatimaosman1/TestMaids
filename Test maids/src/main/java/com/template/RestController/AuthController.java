package com.template.RestController;


import com.template.DTO.JwtResponseDTO;
import com.template.DTO.RegisterResponseDTO;
import com.template.DTO.SignRequestDTO;
import com.template.Model.Basic.User;
import com.template.Security.TokenUtiles;
import com.template.Service.Basic.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/api/v1/auth")
public class AuthController {

    @Autowired
    TokenUtiles tokenUtiles;
    @Autowired
    UserService userService;
    @Autowired
    AuthenticationManager authenticationManager;


    @PostMapping(value = {"/login"},
    consumes = {MediaType.APPLICATION_JSON_VALUE})
    public JwtResponseDTO signIn (@RequestBody SignRequestDTO signRequest){
        final Authentication authentication=authenticationManager.authenticate(
          new UsernamePasswordAuthenticationToken(signRequest.getUsername(),signRequest.getPassword())
        );
        SecurityContextHolder.getContext().setAuthentication(authentication);
        UserDetails userDetails=userService.loadUserByUsername(signRequest.getUsername());
        JwtResponseDTO jwtResponse =new JwtResponseDTO(tokenUtiles.tokenGenerate(userDetails));
        return jwtResponse;
    }

    @PostMapping(value = {"/register"})
    public ResponseEntity<RegisterResponseDTO> Register (@RequestBody User user){
        userService.save(user);
        return new ResponseEntity<>(new RegisterResponseDTO(user.getUsername()), HttpStatus.CREATED);
    }


}
