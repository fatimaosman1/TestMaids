package com.template.RestController;

import com.template.Aspect.RestLog;
import com.template.Exception.InternalServerErrorException;
import com.template.Model.Main.Borrowing;
import com.template.Service.Main.BorrowingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

/**
 * this class is to define the Borrowing Controller Requests.
 */
@RestController
@RequestMapping("/api")
public class BorrowingControler {

	@Autowired
	BorrowingService borrowingService;




	@RestLog(uri = "/api/borrow/{bookId}/patron/{patronId}")
	@PostMapping("/borrow/{bookId}/patron/{patronId}")
	@PreAuthorize("hasAuthority('ADMIN')")
	public ResponseEntity<Borrowing> createBorrowing(@PathVariable("bookId") int bookId,@PathVariable("patronId") int patronId) throws Exception{
			return new ResponseEntity<>(borrowingService.save(bookId,patronId), HttpStatus.CREATED);

	}

	@RestLog(uri = "/api/return/{bookId}/patron/{patronId}")
	@PutMapping("/return/{bookId}/patron/{patronId}")
	@PreAuthorize("hasAuthority('ADMIN')")
	public ResponseEntity<String> updateBorrowing(@PathVariable("bookId") int bookId,@PathVariable("patronId") int patronId) throws Exception {
		borrowingService.update(bookId,patronId);
		return new ResponseEntity<>("return book", HttpStatus.OK);
	}



}
