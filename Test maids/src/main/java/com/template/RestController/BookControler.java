package com.template.RestController;

import com.template.Aspect.RestLog;
import com.template.Exception.InternalServerErrorException;
import com.template.Model.Main.Book;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import com.template.Service.Main.BookService;
import java.util.List;
import java.util.Optional;
/**
 * this class is to define the Book Controller Requests.
 */
@RestController
@RequestMapping("/api")
public class BookControler {

	@Autowired
	BookService bookService;


	@RestLog(uri = "/api/books")
	@GetMapping(value="/books",produces="application/json")
	@PreAuthorize("hasAuthority('ADMIN')")
	public ResponseEntity<List<Book>> getAllBooks() {
		try {
			List<Book> Books = bookService.findAll();;
			if (Books.isEmpty()) {
				return new ResponseEntity<>(HttpStatus.NO_CONTENT);
			}
			return new ResponseEntity<>(Books, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@RestLog(uri = "/api/books/{id}")
	@GetMapping(value="/books/{id}",produces="application/json")
	@PreAuthorize("hasAuthority('ADMIN')")
	public ResponseEntity<Book> getBookById(@PathVariable("id") int id) {
		Optional<Book> BookData = bookService.findBookById(id);
		if (BookData.isPresent()) {
			return new ResponseEntity<>(BookData.get(), HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}


	@RestLog(uri = "/api/books")
	@PostMapping("/books")
	@PreAuthorize("hasAuthority('ADMIN')")
	public ResponseEntity<Book> createBook(@RequestBody Book book) {
		try {
			return new ResponseEntity<>(bookService.save(book), HttpStatus.CREATED);
		} catch (Exception e) {
			throw new InternalServerErrorException(e.getMessage());
		}
	}

	@RestLog(uri = "/api/books/{id}")
	@PutMapping("/books/{id}")
	@PreAuthorize("hasAuthority('ADMIN')")
	public ResponseEntity<Book> updateBook(@PathVariable("id") int id, @RequestBody Book book) {
		Optional<Book> BookData = bookService.findBookById(id);
		if (BookData.isPresent()) {
			return new ResponseEntity<>(bookService.update(book,id), HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}

	@RestLog(uri = "/api/books/{id}")
	@DeleteMapping("/books/{id}")
	@PreAuthorize("hasAuthority('ADMIN')")
	public ResponseEntity<HttpStatus> deleteBook(@PathVariable("id") int id) {
		try {
			bookService.delete(id);
			return new ResponseEntity<>(HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}


}


