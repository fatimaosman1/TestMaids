package com.template.Exception;

public class MethodNotAllowedException extends RuntimeException {
	private static final long serialVersionUID = 1L;

	public MethodNotAllowedException(String param) {

        super(param);
    }
}