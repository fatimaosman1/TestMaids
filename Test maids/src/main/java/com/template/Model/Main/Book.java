package com.template.Model.Main;

import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;

@Entity
@Table(name = "Book")
@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Book {
	
	@Id
	@Column(name="ID")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int ID;
	@NotBlank(message = "title is mandatory")
	@Column(name = "title")
	private String title ;
	@NotBlank(message = "author is mandatory")
	@Column(name = "author")
	private String author;
	@Column(name = "year")
	private long year;
	@Column(name = "ISBN")
	@NotBlank(message = "ISBN is mandatory")
	private String ISBN;
	@Column(name = "Isborrowing")
	private Boolean Isborrowing=false ;
}
