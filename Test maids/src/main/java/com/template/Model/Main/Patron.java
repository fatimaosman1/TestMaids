package com.template.Model.Main;

import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;

@Entity
@Table(name = "Patron")
@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Patron {
	
	@Id
	@Column(name="ID")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int ID;
	@NotBlank(message = "name is mandatory")
	@Column(name = "name")
	private String name ;
	@NotBlank(message = "contact is mandatory")
	@Column(name = "contact_information")
	private String contact;

}
