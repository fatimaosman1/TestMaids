package com.template.Model.Main;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.template.Model.Basic.Action;
import lombok.*;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.util.Date;

@Entity
@Table(name = "Borrowing")
@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Borrowing {
	
	@Id
	@Column(name="ID")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int ID;
	@ManyToOne( optional = true)
	@JoinColumn(name = "patron_id",unique=false, referencedColumnName = "ID")
	private Patron patron ;
	@ManyToOne( optional = true)
	@JoinColumn(name = "book_id",unique=false, referencedColumnName = "ID")
	private Book book ;
	//@NotBlank(message = "date borrowing is mandatory")
	@DateTimeFormat(pattern = "dd-MM-yyyy")
	@Column(name = "date_borrowing")
	private Date dateBorrowing ;
	@DateTimeFormat(pattern = "dd-MM-yyyy")
	@Column(name = "date_retern")
	private Date dateReturen;
	@Version
	private Long version;


	@PrePersist
	void dateBorrowing() {
		this.dateBorrowing =new Date();
	}
	@PreUpdate
	void dateReturen() {
		this.dateReturen = new Date();
	}

}
