package com.template.Model.Basic;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.springframework.format.annotation.DateTimeFormat;


import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity
@Table(name = "Exception_Log_Action")
@Getter(AccessLevel.PUBLIC)
@Setter(AccessLevel.PUBLIC)
@NoArgsConstructor
@ToString
public class ExceptionLogAction {
	
	@Id
	@Column(name="ID")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int ID ;
	@DateTimeFormat(pattern="dd/MM/yyyy")
	@Column(name = "action_date")
	private Date action_date;
	
	@Column(name = "type")
	private int type ;
	
	@Column(name = "description")
	private String description ;


	

	public ExceptionLogAction(  Date action_date, int type, String description) {
		super();
		this.action_date = action_date;
		this.type = type;
		this.description = description;
	}





}
