package com.template.Repository.Basic;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.template.Model.Basic.User;

@Repository 
public interface UsersRep extends JpaRepository<User,Integer>{


	@Query("Select u from User u where u.username=:username")
	public User findByusername(@Param("username")String username);

	
}
