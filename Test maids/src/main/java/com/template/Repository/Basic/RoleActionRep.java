package com.template.Repository.Basic;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.template.Model.Basic.RoleAction;

@Repository
public interface RoleActionRep extends JpaRepository<RoleAction, Integer> {

	@Query("Select a from RoleAction a where a.role.ID=:id  and a.status!=0")
	public List<RoleAction> getRoleActionByRoleId(@Param("id") int id);

    //#{principal.getRole_id()}  to access userDetails object and get current user principal
	@Query("Select a from RoleAction a where a.role.ID= ?#{principal.getRole_id()}  and a.status!=0")
	public List<RoleAction> getRoleActionByuser();

}
