package com.template.Repository.Main;


import com.template.Model.Main.Patron;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface PatronRep extends JpaRepository<Patron,Integer>{
}

