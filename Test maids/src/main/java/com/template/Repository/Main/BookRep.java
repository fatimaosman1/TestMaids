package com.template.Repository.Main;


import com.template.Model.Basic.Action;
import com.template.Model.Main.Book;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface BookRep extends JpaRepository<Book,Integer>{
}

