package com.template.Repository.Main;


import com.template.Model.Main.Borrowing;
import com.template.Model.Main.Patron;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.Optional;

@Repository
public interface BorrowingRep extends JpaRepository<Borrowing,Integer>{
	@Modifying
	@Query("update Borrowing u set u.dateReturen = :dateReturen where u.book.ID = :bookid and u.patron.ID=:patronid")
	void updateBorrowingdateReturen(@Param("dateReturen") Date dateReturen, @Param("bookid") int bookid,@Param("patronid") int patronid);
	@Query(value = "select  u.* from  Borrowing u where u.book_id = :bookid and u.patron_id=:patronid and u.date_retern is null LIMIT 1",nativeQuery = true)
	Optional<Borrowing> findtbyBookANAndPatron(@Param("bookid") int bookid, @Param("patronid") int patronid);

}

