package com.template.Advisor;

import lombok.*;
import org.springframework.http.HttpStatus;
@Getter(AccessLevel.PUBLIC)
@Setter(AccessLevel.PUBLIC)
@NoArgsConstructor
@AllArgsConstructor
public class ErrorResponse {

    private HttpStatus status;
    private String message;


}
